#!/bin/sh

# requires 8GB GPU GRAM
python train.py --dataset cakes_voc --model psp --backbone resnet50 --epochs 10000 --lr 0.001 --checkname res50 --syncbn --workers 4 --batch-size 4 --resume runs/cakes_voc/psp/res50/checkpoint.params 
