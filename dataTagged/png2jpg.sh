#!/bin/sh
for i in *.png;do jp=`echo $i|sed -e 's/png/jpg/g'` && convert  $i $jp;done

# json 11.png to 11.jpg
for i in *.json;do sed -i 's:png":jpg":' $i;done
