======================================
 MXNET GluonCV 分割实例
======================================

## 0. 标记数据

使用 labelme ，来自 https://github.com/wkentaro/labelme

```
    pip install labelme
```

标记图片，保存为json格式, 请确保你所有的图片都是jpg格式. 标记完成后，我们使用脚本

https://github.com/wkentaro/labelme/blob/master/examples/semantic_segmentation/labelme2voc.py

来转化为 VOC 分割格式.

运行labelme2voc.py之前准备一个 labels.txt , 内容如下

```
__ignore__
_background_
cake
```
你的分类名字必须在 `_background_` 的下方,然后上面两行的内容不要动，

然后这样转换,

```
python labelme2voc.py --labels  labels.txt YourTaggedDir  VoCFormatOutputDir
```

运行之后，在目录VoCFormatOutputDir里面运行命令创建目录

```
    mkdir -p ImageSets/Segmentation
```

然后到Segmentation目录下面，准备 train.txt, val.txt, trainval.txt, test.txt
这些文件里面都是图片的名称前缀，不包含.jpg
例如，我有1-100.jpg, 然后train.txt就像这样:

```
1
2
3
4
...
51
52
53
```

我放了 54-70 到 val.txt, 然后 trainval.txt 就是train.txt + val.txt的内容了.

在 test.txt里面, 我放置了 71-100.

## 1. 安装 mxnet-cuda 版本和 gluoncv

 ```
  pip install mxnet-cu100
  pip install gluoncv
 ```
 
## 2. 安装 cakes_voc类 到 GluonCV model zoo

 复制目录 `cakes_voc` 到 PYTHONENV/lib/python3.5/site-packages/gluoncv/data
 然后编辑  `PYTHONENV/lib/python3.5/site-packages/gluoncv/data/__init__.py`
 
 增加cakes_voc的内容:
 
 ``` python
    from .mixup.detection import MixupDetection
    from .cakes_voc.segmentation import CakesVOCSegmentation

    datasets = {
        'ade20k': ADE20KSegmentation,
        'pascal_voc': VOCSegmentation,
        'pascal_aug': VOCAugSegmentation,
        'coco' : COCOSegmentation,
        'citys' : CitySegmentation,
        'cakes_voc' : CakesVOCSegmentation,
    }
 ```
 
## 3. 训练

 source gluoncv_env/bin/activate
 sh train.sh
 
 注意: 首次训练，将 train.sh里面的 --resume及参数去掉
 
## 4. 测试

 source gluoncv_env/bin/activate
 sh test.sh
